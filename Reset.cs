﻿using NAND_Prog;
using System.ComponentModel.Composition;

namespace Reset_FFh
{
    //Ця DLL експортує операцію Reset для чіпа

    [Export(typeof(Operation)),
        ExportMetadata ("Name" , "Reset_FFh")]
    public class Reset : Operation
    {
        public Reset()
        {
            name = "Reset";
            based = typeof(Chip);

            Instruction instruction;
            //--------------------------------------------------------------
            instruction = new WriteCommand();                              //Створюю інструкцію WriteCommand
            chainInstructions.Add(instruction);                            //Додаю її в ланцюжок інструкцій операції Reset

            //    instruction.numberOfcycles = 0x01;                           //По дефолту 1       
            (instruction as WriteCommand).command = 0xFF;
            (instruction as WriteCommand).Implementation = GetCommandMG;
            //--------------------------------------------------------------

        }
    }
}
